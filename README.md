# Oauthable Axios Salesforce HTTP client

## Getting Started

### Installation

`$ npm install @intofilm/salesforce-client`

### Usage

```javascript
const SalesforceClient = require("@intofilm/salesforce-client");

const args = {
  baseURL: "https://predator.my.salesforce.com/services/data/v49.0",
  client_id: "abcdefg1234567890",
  client_secret: "abcdefg1234567890",
  username: "test@example.com",
  password: "correct-horse-battery-staple",
  oauthURL: "https://test.salesforce.com"
};

const client = SalesforceClient.create(args);

client.get(/*<axios style args>*/).then((data) => {});
```

Where required (in an AWS Lambda for example) it's preferable to pass the client as a Promise.

```javascript
const SalesforceClient = require("@intofilm/salesforce-client");

const clientPromise = SalesforceClient.promise({...});

clientPromise.then((client) => client.get(/*<axios style args>*/)).then();
```
