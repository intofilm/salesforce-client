const axios = require("axios");
const qs = require("qs");

// https://help.salesforce.com/articleView?id=remoteaccess_oauth_endpoints.htm&type=5
// https://help.salesforce.com/articleView?id=remoteaccess_oauth_username_password_flow.htm&type=5

module.exports = ({ url, ...credentials }) =>
  axios({
    url,
    data: qs.stringify({ ...credentials, grant_type: "password" }),
    headers: {
      Accept: "application/json"
    },
    method: "post"
  })
    .then((res) => res.data)
    .catch(({ response }) =>
      Promise.reject({
        response,
        code: response.status,
        message: `OAuth error: ${response.data.error || "Unknown"} "${
          response.data.error_description || "Unknown"
        }"`
      })
    );
