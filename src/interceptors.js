const tokenProvider = require("axios-token-interceptor");

const errors = ({ errors }) =>
  errors ? errors.map((e) => `${e.message} (${e.statusCode})`) : ["Unknown"];

const apexError = ({ response, code, message }) =>
  Promise.reject(
    new Error(
      response && response["data"] && response["data"][0]
        ? `[${response.status}] ${errors(response.data[0]).join(" ... ")}`
        : `[500] ${message} (${code})`
    )
  );

const headerFormatter = (body) => `Bearer ${body.access_token}`;
const getMaxAge = (body) => 3600 * 1000;

const oauthRequest = (getToken) =>
  tokenProvider({
    getToken: tokenProvider.tokenCache(getToken, { getMaxAge }),
    headerFormatter
  });

const traceResponse = (response) => {
  if (process.env["TRACE"]) {
    // console.debug(`http status: ${response.status}`);
    console.log(JSON.stringify(response.data));
  }
  return response;
};

const traceRequest = ({ url, data, ...request }) => {
  if (process.env["TRACE"]) {
    console.log(`Calling: ${url} with ${JSON.stringify(data)}`);
  }
  return { url, data, ...request };
};

module.exports = {
  apexError,
  oauthRequest,
  traceResponse,
  traceRequest
};
