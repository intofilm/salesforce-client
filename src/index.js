const axios = require("axios");
const oauthClient = require("./oauth-client");
const {
  oauthRequest,
  apexError,
  traceResponse,
  traceRequest
} = require("./interceptors");

const create = (config) => {
  const { baseURL, client_id, client_secret, username, password, oauthURL } =
    config;

  const instance = axios.create({
    baseURL,
    headers: { "Content-Type": "application/json" }
  });

  instance.interceptors.request.use(
    oauthRequest(() =>
      oauthClient({
        url: oauthURL + "/services/oauth2/token",
        client_id,
        client_secret,
        username,
        password
      })
    )
  );

  instance.interceptors.response.use(traceResponse, apexError);
  instance.interceptors.request.use(traceRequest);

  return instance;
};

const promise = (config) => new Promise((resolve) => resolve(create(config)));

module.exports = { create, promise };
