const { apexError } = require("../interceptors");

it("should generate an unknown error if no errors are present", () => {
  const e = {
    code: 666,
    message: "Boom!",
    response: {
      status: 400,
      statusText: "Bad Request",
      data: []
    }
  };

  expect.assertions(1);

  return apexError(e)
    .then(console.log)
    .catch((err) => {
      expect(err.message).toEqual(`[500] ${e.message} (${e.code})`);
    });
});

it("throws an error for a bad http status code", () => {
  // This is an example message from apex 🤦‍♂️
  const response = require("../__fixtures__/unknown-error.json");

  expect.assertions(1);

  return apexError({ response }).catch((err) => {
    expect(err.message).toEqual(
      "[400] An error occurred when executing a flow interview. (UNKNOWN_EXCEPTION)"
    );
  });
});

it("should throw something sensible if no response is returned (like connection refused)", () => {
  const e = {
    message: "connect ECONNREFUSED 127.0.0.1:80",
    code: "ECONNREFUSED"
  };

  expect.assertions(1);

  return apexError(e).catch((err) => {
    expect(err.message).toEqual(
      "[500] connect ECONNREFUSED 127.0.0.1:80 (ECONNREFUSED)"
    );
  });
});
